[[ ! -e /.dockerenv ]] && exit 0 set -xe

apt-get update -yqq
apt-get install git -yqq

curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
chmod +x /usrlocal/bin/phpunit

docker-php-et-install pdo_mysql
